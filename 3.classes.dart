void main(){
  var app1 = new App("fnb","Banking","MTN contestants",2017);
  app1.printData();
  app1.capitalizeName();
}

class App{
  String name = '';
  String sector = '';
  String developer = '';
  int year = 0; 

  App(String name,String sector, String developer, int year){
    this.name = name;
    this.developer = developer;
    this.sector = sector;
    this.year = year;
  }

  void printData(){
    print('Name : ${this.name.toUpperCase()} \nSector : ${this.sector} \nDeveloper : ${this.developer} \nYear : ${this.year}');
  }

  void capitalizeName(){
    print(this.name.toUpperCase());
  }
}